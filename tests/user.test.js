const request = require("supertest");
const app = require("./../index");

let user;

describe("user API", () => {
    beforeEach(() => {
        jest.setTimeout(1000);
    });

    test("GET /users", (done) => {
        request(app)
            .get("/users")
            .expect(200)
            .expect((res) => {
                res.status = "success";
            })
            .end((err, res) => {
                if (err) return done(err);
                return done();
            });
    });

    // Hidden for simplicity
    test("POST /users", (done) => {
        request(app)
            .post("/users")
            .expect("Content-Type", /json/)
            .send({
                "name": "Vinod Choudhari",
                "email": "vinodchoudhari90@gmail.com",
                "dob": "1990-08-30"
            })
            .expect(200)
            .expect((res) => {
                res.status = "success";
                res.body.data.length = 1;
                res.body.data.email = "vinodchoudhari90@gmail.com";
            })
            .end((err, res) => {
                if (err) return done(err);
                user = res.body.data._id;
                return done();
            });
    });

    // Hidden for simplicity
    test("PUT /users/:id", (done) => {
        request(app)
            .put(`/users/${user}`)
            .expect("Content-Type", /json/)
            .send({
                "name": "Vinod Choudhari",
                "email": "vchoudhari@yopmail.com",
                "dob": "1990-08-30"
            })
            .expect(200)
            .expect((res) => {
                res.status = "success";
                res.body.data.length = 1;
                res.body.data.email = "vchoudhari@yopmail.com";
                res.body.data._id = user;
            })
            .end((err, res) => {
                if (err) return done(err);
                return done();
            });
    });
    // More things come here

    test("DELETE /users/:id", (done) => {
        request(app)
            .delete(`/users/${user}`)
            .expect(204)
            .end((err, res) => {
                if (err) return done(err);
                return done();
            });
    });
});