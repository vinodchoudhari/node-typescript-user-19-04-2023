# node-typescript-user-19-04-2023

## Technical Stacks
- [ ] NodeJS 
- [ ] TypeScript 
- [ ] MongoDB
- [ ] Jest

## NPM Packages
- [ ] Express 
- [ ] Mongoose 
- [ ] Axios
- [ ] Nodemon
 
## NPM Commands
- [ ] npm install - to install all packages 
- [ ] npm run start - to run setup
- [ ] npm run test - to test api using jest
