import http from 'http';
import express, { ErrorRequestHandler, Express } from 'express';
import morgan from 'morgan';
import user_routes from './src/routes/users';
import db from './src/configs/db-config';
import response from './src/helpers/response';

const app: Express = express();

/** setup basic json and logging libraries */
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

/** setup database */
db();

/** setup cors */
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'origin, X-Requested-With,Content-Type,Accept, Authorization');
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'GET PATCH DELETE POST');
        return res.status(200).json({});
    }
    next();
});

/** setup api routes */
app.use('/users', user_routes);

/** setup error handling mechnism */
app.use((err: Error, req: any, res: any, next: any) => {
    return res.status(404).json(response.error(err.message));
});

/** Setup server running configuration */
const httpServer = http.createServer(app);
httpServer.listen(process.env.PORT ?? 3000, () => console.warn(`Node TypeScript Quick Setup Running...`));

export = app;