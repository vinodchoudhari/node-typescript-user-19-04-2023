import express from 'express';
const router = express.Router();
import user_controller from "../controllers/user";

/** User routes here */
router.get('/', user_controller.list);
router.get('/:id', user_controller.detail);
router.put('/:id', user_controller.update);
router.delete('/:id', user_controller.remove);
router.post('/', user_controller.create);

export = router;