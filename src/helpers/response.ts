const success = (data: any, message: String) => {
    return {
        message: message,
        status: "success",
        data: data
    }
}

const error = (message: String) => {
    return {
        message: message,
        status: "error"
    }
}

export = { success, error }