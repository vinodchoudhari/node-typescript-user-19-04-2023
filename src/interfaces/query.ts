interface IQueryParams {
    page: String | undefined;
    pageSize: String | undefined;
    search: String | undefined;
    fields: String | undefined;
    sort: String | undefined;
}
export = IQueryParams;