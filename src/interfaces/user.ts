interface IUser {
    name: String;
    email: String;
    dob: Date;
}

export = IUser;