import IQueryParams from "../interfaces/query";
import IUser from "../interfaces/user";
import User from "../models/user";

const createUser = async (user: IUser) => {
    return await new User(user).save();
}

const getUsers = async (query?: IQueryParams) => {
    return await User.find({});
}

const getUser = async (id: String, query?: IQueryParams) => {
    return await User.findById(id);
}

const updateUser = async (id: String, user: IUser, query?: IQueryParams) => {
    return await User.findByIdAndUpdate(id, user);
}

const removeUser = async (id: String, query?: IQueryParams) => {
    return await User.findByIdAndDelete(id);
}

export = { createUser, getUsers, getUser, updateUser, removeUser }