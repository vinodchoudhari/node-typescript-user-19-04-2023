import { connect } from 'mongoose';
import env_config from "./../configs/env-config";


export = async () => {
    const URI = 'mongodb+srv://' + env_config.DB_USERNAME + ':' + env_config.DB_PASSWORD + '@' + env_config.DB_CLUSTER + '/' + env_config.DB_NAME;
    await connect(URI);
    console.warn("database connected...");
}