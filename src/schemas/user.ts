import { Schema } from 'mongoose';
import IUser from './../interfaces/user';
const userSchema = new Schema<IUser>({
    name: { type: String, required: true },
    email: { type: String, required: true },
    dob: { type: Date, required: true }
});

export = userSchema;