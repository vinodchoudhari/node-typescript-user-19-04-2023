import { Request, Response, NextFunction } from 'express';
import user_service from './../services/user';
import IUser from '../interfaces/user';
import response from './../helpers/response';
import IQueryParams from '../interfaces/query';

const create = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user: IUser = req.body;
        return res.status(200).json(response.success(await user_service.createUser(user), "user created successfully"));
    } catch (error) {
        next(error);
    }
}

const list = async (req: Request, res: Response, next: NextFunction) => {
    try {
        return res.status(200).json(response.success(await user_service.getUsers(), "users list fetch successfully"));
    } catch (error) {
        next(error);
    }
}

const detail = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const id: String = req.params.id;
        return res.status(200).json(response.success(await user_service.getUser(id), "user detail fetch successfully"));
    } catch (error) {
        next(error);
    }
}

const update = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user: IUser = req.body;
        const id: String = req.params.id;
        return res.status(200).json(response.success(await user_service.updateUser(id, user), "user updated successfully"));
    } catch (error) {
        next(error);
    }
}

const remove = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const id: String = req.params.id;
        return res.status(204).json(response.success(await user_service.removeUser(id), "user deleted successfully"));
    } catch (error) {
        next(error);
    }
}

export default { create, list, detail, update, remove };
